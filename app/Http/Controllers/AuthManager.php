<?php

namespace App\Http\Controllers;

use App\Models\User;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthManager extends Controller
{
    function dashboard(){
        if(Auth::check()){
            return redirect(route('login'));
        }
        return view('login');
    }
    function login(){
        if(Auth::check()){
            return redirect(route('dashboard'));
        }
        return view('login');
    }

    function register(){
        if(Auth::check()){
            return redirect(route('dashboard'));
        }
        return view('register');
    }

    function loginPost(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials)){
            return redirect()->intended(route('dashboard'));
        }
        return redirect(route('login'))->with("error", "Login details are not valid");
    }

    function registerPost(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);
        $user = User::create($data);
        if(!$user){
            return redirect(route('register'))->with("error", "registrasi failed");
        }
        return redirect(route('login'))->with("success", "Registrasi Success, go to login");
     }

     function logout(){
        Session::flush();
        Auth::logout();
        return redirect(route('login'));
     }

}
